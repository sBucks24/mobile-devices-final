package com.example.a100555289.finalproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class HighScoreActivity extends AppCompatActivity {

    //Ref to list view
    ListView scoreListView;

    //Ref to strings.xml
    String n;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

       // MainActivity.scoresList = MainActivity.database.getAllScores();

        n = getString(R.string.player);
        s = getString(R.string.score);

        scoreListView = (ListView) findViewById(R.id.listView);
        populateList();
    }

    //fill the listview
    private void populateList()
    {
        ArrayAdapter<Score> adapter = new ScoreListAdapter();
        scoreListView.setAdapter(adapter);
    }

    private class ScoreListAdapter extends ArrayAdapter<Score> {
        public ScoreListAdapter() {
            super(HighScoreActivity.this, R.layout.listview_scores, MainActivity.scoresList);
        }

        public View getView(int position, View view, ViewGroup parent)
        {
            if(view == null)
            {
                //instantiate layout xml
                view = getLayoutInflater().inflate(R.layout.listview_scores, parent, false);
            }
            //get contact to display
            Score currentScore = MainActivity.scoresList.get(position);

            TextView player = (TextView) view.findViewById(R.id.player);
            player.setText(currentScore.getUser());

            TextView score = (TextView) view.findViewById(R.id.score);
            score.setText(String.valueOf(currentScore.getScore()));

            TextView city = (TextView) view.findViewById((R.id.city));
            city.setText(currentScore.getCity());

            return view;
        }
    }

    public void backButtonClicked(View view)
    {
        finish();
    }
}
