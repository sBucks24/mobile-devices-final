package com.example.a100555289.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class InstructionsActivity extends AppCompatActivity {

    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        Intent i = getIntent();
        String text = i.getStringExtra("data");
        //String text = "hello";
        txt=(TextView)findViewById(R.id.text_view);
        txt.setText(text);
    }

    public void close(View view)
    {
        finish();
    }

}
