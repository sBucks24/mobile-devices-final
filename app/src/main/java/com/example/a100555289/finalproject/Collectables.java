package com.example.a100555289.finalproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.Random;

/**
 * Created by 100517424 on 11/25/2016.
 */
public class Collectables {
    //sprite
    private Bitmap bitmap;

    //Where the dots spawn and direction they move
    private int x;
    private int y;
    //took out cause lag
    //private int xSpeed;
    //private int ySpeed;

    //DOnt let them spawn to off screen
    private int maxX;
    private int maxY;

    //destroy the collectable
    private boolean isDead;
    private int lifeTime;

    //points worth
    private int points = 0;

    public Collectables(Context context, int screenX, int screenY)
    {
        lifeTime = 50;

        //Random generator to determine properties of collectable
        Random generator = new Random();

        //Determine if the collectable is good or not. 1 in 4 are bad
        int type = generator.nextInt(4);
        if (type == 3){
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.collectable2);
            points = -1;}
        else {
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.collectable1);
            points = 1;
        }

        maxX = screenX - bitmap.getWidth();
        maxY = screenY - bitmap.getHeight();



        x = generator.nextInt(maxX);
        y = generator.nextInt(maxY);

        //Took out cause lag
//        xSpeed = generator.nextInt(100);
//        ySpeed = generator.nextInt(100);

        isDead = false;
    }
    //delete the collectable
    public void setDead(boolean _isDead)
    {
        isDead = _isDead;
    }

    public boolean getDead()
    {
        return isDead;
    }

    public void update()
    {
        //took out cause lag
     //   x += xSpeed;
     //   y += ySpeed;

        //Check lifespan
        if(lifeTime != 0)
            lifeTime--;
        else
            isDead = true;

        //if they move offscreen, kill them
        if (x < -100 || y < -100 || x > maxX || y > maxY)
            isDead = true;


    }


    public Bitmap getBitmap()
    {
        return bitmap;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getPoints()
    {
        return points;
    }

}
