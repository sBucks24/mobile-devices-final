package com.example.a100555289.finalproject;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    //Database of scores
    public static ScoreDBHelper database;
    public static List<Score> scoresList;
    public static int score;

    //get city of player
    String city = "No City";

    //Avoid duplicate score/names being added to highscores
    private String previousName;
    private int previousScore;

    LocationManager locationManager;
    String provider;

    //testing sql
    EditText playerText;
    TextView scoreText;

    //store credits downloaded
    public static String instructions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = new ScoreDBHelper(this);
        scoresList = database.getAllScores();

        playerText = (EditText) findViewById(R.id.playerTxt);


        instructions = "";
        //new DownloadActivity().execute("http://dragonslothstudios.com/mobiledevices");
        new DownloadActivity().execute("http://m.uploadedit.com/ba3s/1480361321348.txt");
       // scoreText = (TextView) findViewById(R.id.scoreTxt);



        //Geocoding stuff
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        Location location;
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else{
            location = locationManager.getLastKnownLocation(provider);
            if (location != null){
                onLocationChanged(location);
            }
        }

    }

    @Override
    public void onResume(){
        super.onResume();


        //Geocoding, adding permission stuff
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else
        {
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);
            locationManager.requestLocationUpdates(provider, 400, 1, this);
        }

        //Check for duplicate names/strings
        if (previousName != playerText.getText().toString() && previousScore != score)
            sendHighscore();


        //Return with update list of scores
        scoresList = database.getAllScores();

    }

    //when start button is clicked, go to game
    public void sendHighscore()
    {
        //Update last name/score
        previousName = playerText.getText().toString();
        previousScore = score;

        //When user returns to main, send their highscore to the database
        database.insertScore(playerText.getText().toString(), String.valueOf(score),city);

    }
    public void startButtonClicked(View view)
    {
        //Go to game
        Intent i = new Intent(this, GameActivity.class);
        startActivity(i);
    }

    //when highscore button is clicked, go to high score activity
    public void highScoreButtonClicked(View view)
    {
        Intent i = new Intent(this, HighScoreActivity.class);
        startActivity(i);
    }

    public void instructionsButtonClicked(View view)
    {
        Intent i = new Intent(this, InstructionsActivity.class);
        i.putExtra("data", instructions);
        startActivity(i);
    }


    //Geocoding stuff to get the city name
    public void onLocationChanged(Location location)
    {
        double lat = (location.getLatitude());
        double lng = (location.getLongitude());

        changeAddress(lat, lng);
    }
    public void onStatusChanged(String provider, int status, Bundle extras) {    }
    public void onProviderEnabled(String provider) { Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();  }
    public void onProviderDisabled(String provider){ Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();     }

    public void changeAddress(double lat, double lng)
    {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            city = addresses.get(0).getLocality();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


}
