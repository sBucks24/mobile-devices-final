package com.example.a100555289.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by 100555289 on 11/25/2016.
 */
public class ScoreDBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "ScoresData.db";
    public static final String TABLE_NAME = "score_table";
    public static final String KEY_ID = "ID";
    public static final String KEY_USER = "USER";
    public static final String KEY_SCORE = "SCORE";
    public static final String KEY_CITY = "CITY";


    public ScoreDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,USER TEXT,SCORE INTEGER,CITY TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //Take function arguments and put them into database , add score
    public boolean insertScore(String user, String score, String city)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //Assign table values with arguments
        contentValues.put(KEY_USER, user);
        contentValues.put(KEY_SCORE, score);
        contentValues.put(KEY_CITY, city);

        long result = db.insert(TABLE_NAME, null, contentValues);
        db.close();
            //check if it inserted
            if(result == -1)
            {
            return false;
        }
        else return true;
    }

    //delete score
    public void deleteScore(Score score){
        SQLiteDatabase db= this.getWritableDatabase();
        //delete score in table with id that equals to the argument product id
        db.delete(TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(score.getId())});
        db.close();
    }

    //return a list of scores
    public List<Score> getAllScores(){
        List<Score> scoreList= new ArrayList<Score>();

        //find everything from the table
        String query = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        //increment through all the scores in table and place them into list
        if(cursor.moveToFirst()){
            do{
                Score score = new Score(Integer.parseInt(cursor.getString(0)), cursor.getString(1), Integer.parseInt(cursor.getString(2)), cursor.getString(3));
                scoreList.add(score);
            }while(cursor.moveToNext());
        }

        //Sort list
       // if(scoreList.size() > 5)
        // {
            Collections.sort(scoreList, new Comparator<Score>() {
                @Override
                public int compare(Score s1, Score s2) {
                    return s2.getScore() - s1.getScore();
                }
           });
       // }

        return scoreList;
    }

}
