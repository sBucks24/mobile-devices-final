package com.example.a100555289.finalproject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 100517424 on 11/25/2016.
 */
public class GameView extends SurfaceView implements Runnable {
    volatile boolean playing;

    private Thread gameThread = null;

    //Collectables and player input
    private ArrayList<Collectables> collectables;
    private float playerX;
    private float playerY;

    //Screen size
    private int sX;
    private int sY;

    //Score and game timer
    private int timer = 250;

    //Drawing stuff
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    final MediaPlayer pingSound = MediaPlayer.create(getContext(), R.raw.ping);


    public GameView(Context context, int screenX, int screenY)
    {
        super(context);

        //set screen size
        sX = screenX;
        sY = screenY;

        //Game timer and score sets
        timer = 250;
        MainActivity.score = 0;

        //Set up collectables array
        collectables = new ArrayList<Collectables>();
        Collectables newCollect = new Collectables(context, screenX, screenY);
        collectables.add(newCollect);

        //Paint stuff
        surfaceHolder = getHolder();
        paint = new Paint();

    }

    @Override
    public void run()
    {
        while(playing)
        {
            update();

            draw();

            control();
        }
    }

    public void update()
    {
        if (collectables.size() > 0)
            for (int i = 0; i < collectables.size(); i++)
            {
                collectables.get(i).update();
                if (collectables.get(i).getDead())
                    collectables.remove(i);
            }

        //end game
        if (timer == 0)
        {
            playing = false;
            //Finish();
           // setContentView(GameActivity);
        }
        //countdown
        else {
            Collectables newCollect = new Collectables(getContext(), sX, sY);
            collectables.add(newCollect);

            timer--;
        }
    }


    public void draw()
    {
        if (surfaceHolder.getSurface().isValid()){
            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.WHITE);

            //Draw each active collectable
            if (collectables.size() > 0)
                for (int i = 0; i < collectables.size(); i++)
                     canvas.drawBitmap(collectables.get(i).getBitmap(),
                                      collectables.get(i).getX(),
                                      collectables.get(i).getY(),
                                      paint);

            paint.setColor(Color.BLACK);
            paint.setTextSize(150);
            canvas.drawText(String.valueOf(MainActivity.score), 50, 150, paint);

            if (playing == true)
                canvas.drawText(String.valueOf(timer), sX/2-100, sY-100, paint);
            else {
                canvas.drawText("Go Back", sX / 2 - 150, sY - 100, paint);
            }
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }
    public void control()
    {
        try {
            gameThread.sleep(17);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        switch(motionEvent.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                playerX = motionEvent.getX();
                playerY = motionEvent.getY();

                checkForCollect();

                if (playing == false)
                    gameThread.interrupt();
                break;
        }
        return true;
    }

    public void checkForCollect()
    {
        if (collectables.size() > 0)
            for (int i = 0; i < collectables.size(); i++)
            {
                if (playerX > collectables.get(i).getX() && playerX < collectables.get(i).getX() + collectables.get(i).getBitmap().getWidth()
                    && playerY > collectables.get(i).getY() && playerY < collectables.get(i).getY() + collectables.get(i).getBitmap().getHeight())
                {
                    collectables.get(i).setDead(true);
                    MainActivity.score += collectables.get(i).getPoints();
                    pingSound.start();
                }
            }
    }

    public void pause(){
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e){

        }
    }

    public void resume(){
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
}
