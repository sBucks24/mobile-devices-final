package com.example.a100555289.finalproject;

/**
 * Created by 100555289 on 11/25/2016.
 */
public class Score {
    private int id;
    private String user;
    private int score;
    private String city;

    public Score(int _id, String _user, int _score, String _city)
    {
        id = _id;
        user = _user;
        score = _score;
        city = _city;
    }

    public int getId(){return id;}
    public String getUser(){return user;}
    public int getScore(){return score;}
    public String getCity(){return city;}

}
