Mobile Devices Final Project: Click It

Samuel Buckle - 100517424
Darian Tse - 100555289

The project should install and ask for permission to access locations.

The point of the game is to click as many blue dots as possible without pressing red ones.
Once the game is complete, press the back arrow on the phone and go to highscores to see
the database of local highscores and where they were done.


Activities/Intents: Main, game, highscores, instructions
Internet resource: Instructions page
Database access: Highscores
Media Playback: pick ups
Graphics: Drawing, menu and game
Geocoding: Where the player is when registering a highscore